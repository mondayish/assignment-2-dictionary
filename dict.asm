extern string_equals

section .text

%define ELEMENT_LENGTH 8
%define FOUND_FLAG 1

global find_word

; rdi - ключ, rsi - начало словаря
find_word:
	.loop:
		add rsi, ELEMENT_LENGTH
		push rdi
		push rsi
		call string_equals
        pop rsi
		pop rdi
		sub rsi, ELEMENT_LENGTH

		cmp rax, FOUND_FLAG
		je .found
		mov rsi, [rsi]
		cmp rsi, 0
		jne .loop

	.not_found:
		xor rax, rax
		ret

	.found:
		mov rax, rsi
		ret