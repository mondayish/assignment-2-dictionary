global _start

extern find_word
extern read_word
extern print_string
extern print_newline
extern exit

%define BUF_SIZE 256
%define ELEMENT_LENGTH 8

section .rodata
    not_found: db "Not found", 0
    buffer_overflow: db "Buffer overflow", 0

section .bss
    buffer: times BUF_SIZE db 0

section .text
_start:
	mov rdi, buffer
	mov rsi, BUF_SIZE
	call read_word
	cmp rax, 0
	je .buffer_ovfw

	mov rdi, rax
	mov rsi, NEXT_ELEMENT
	push rdx
	call find_word
	pop rdx
	cmp rax, 0
	je .not_found

    .found:
        mov rdi, rax
        add rdi, ELEMENT_LENGTH
    	add rdi, rdx
    	inc rdi
    	jmp .success

	.not_found:
	    mov rdi, not_found
	    jmp .error

	.buffer_overflow:
	    mov rdi, buffer_overflow
        jmp .error

    .error:
        call print_string
        call print_newline
        mov rdi, 1
        call exit

	.success:
	    call print_string
	    call print_newline
	    xor rdi, rdi
	    call exit